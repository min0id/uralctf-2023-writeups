# One little dump - Saga of Ctrl+c/Ctrl+v

**Условие задания:**
> А вы знакомы с [volatility](https://www.volatilityfoundation.org/)?
Мой компьютер выключился, и я потерял все свои данные.
Но я успел скопировать очень важную информацию, помогите мне восстановить её.
[dump.tar.gz](https://disk.yandex.ru/d/7_IVy__FIBCevQ)

**Решение:**
После распаковки архива видим `dump.raw` файл. В сочетании с [описанием таска](https://www.volatilityfoundation.org/) понимаем, что перед нами [дамп оперативной памяти](https://en.wikipedia.org/wiki/Core_dump) операционной системы.

С помощью плагина `imageinfo` для volatility 2.6.1 узнаем операционную систему, с которой сняли данный дамп.
```
volatility.exe -f dump.raw imageinfo
```
И получаем вывод команды, где видим, что, скорее всего, перед нами дамп оперативной памяти, который был снят с `Windows 7 SP1 x64`

>Volatility Foundation Volatility Framework 2.6.1
Suggested Profile(s) : **`Win7SP1x64`**, Win7SP0x64, Win2008R2SP0x64, Win2008R2SP1x64_24000, Win2008R2SP1x64_23418, Win2008R2SP1x64, Win7SP1x64_24000, Win7SP1x64_23418

После того, как мы узнали профиль дампа и посмотрели на заголовок таска, замечаем, что, вероятно, речь идёт о содержимом буфера обмена.

Используем плагин `clipboard`:
```
volatility.exe -f dump.raw --profile=Win7SP1x64 clipboard
```
И получаем вывод команды, где видим в столбце **Data** строку в какой-то кодировке:
| Session | WindowStation | Format | Handle | Object | Data |
| ------ | ------ | ------ | ------ | ------ | ------ |
| 1 | WinSta0 | CF_UNICODETEXT | 0x3001f7 | 0xfffff900c07c35a0 | **6kYpA7qEPwbXnYpZJTw1K4WYHSuFgCNCT2qzYtP9bpTn** |

Для определения кодировки и получения флага воспользуемся любым онлайн сервисом, например, [CyberChef](https://gchq.github.io/CyberChef/).

Видим, что данная строка была закодирована [Base58](https://ru.wikipedia.org/wiki/Base58), декодируем и получаем флаг:
``UralCTF{v0l471l17y_15_my_w34p0n}``