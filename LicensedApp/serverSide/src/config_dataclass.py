import configparser
from dataclasses import dataclass


@dataclass
class ConfigData:
    config = configparser.ConfigParser()
    config.read("config.ini")
    config_address = str(config["DEFAULT"]["address"])
    config_port = int(config["DEFAULT"]["port"])

    flag = config["SECRETS"]["flag"]
    
    f = open("key", "r")
    config_key = f.read()
    f.close()

    f = open("license", "r")
    config_license = f.read()
    f.close()
