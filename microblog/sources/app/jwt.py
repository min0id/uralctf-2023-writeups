import json
import jwt
from base64 import b64decode, urlsafe_b64decode

from typing import List

def pad_base64(data):
    mod = len(data) % 4
    if mod == 0:
        return data
    elif mod == 2:
        return data + '=='
    elif mod == 3:
        return data + '='
    raise Exception("JWT Validation error")

def b64dec_str(_input: str):
    padded = pad_base64(_input)
    return b64dec(bytes(padded, encoding='ascii'))

def b64dec(_input: bytes):
    return b64decode(_input, b'-_').decode()

def decode(_jwt: str, secret: str):
    partitions = _jwt.split(".")
    header = json.loads(b64dec_str(partitions[0]))
    algorithm = header["alg"]
    is_okay = lambda x: x != "none"
    return jwt.decode(_jwt, secret if is_okay(algorithm) else "", algorithm, options={"verify_signature": is_okay(algorithm)})