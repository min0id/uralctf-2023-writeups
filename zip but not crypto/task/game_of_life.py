import time, random, pygame

pygame.init()
game_size = 100
k_size = 10
size = game_size*k_size, game_size*k_size
a = [[False for i in range(game_size)] for j in range(game_size)]
new_a = [[False for i in range(game_size)] for j in range(game_size)]
screen = pygame.display.set_mode(size)
notPaused = False
x = 0
y = 0

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: quit()
        elif event.type == pygame.MOUSEBUTTONDOWN: 
            x = event.pos[0] // k_size
            y = event.pos[1] // k_size
            if event.button == 3: a[x][y] = False
            elif event.button == 1: a[x][y] = True
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE: notPaused = not notPaused
            if event.key == pygame.K_r: 
                for row in a:
                    for element in row:
                        if random.randint(0,1) == 1: a[x][y] = True
                        else: a[x][y] = False
                        y += 1 
                    x += 1
                    y = 0
                x = 0
            if event.key == pygame.K_ESCAPE: 
                for row in a:
                    for element in row:
                        a[x][y] = False
                        y += 1 
                    x += 1
                    y = 0
                x = 0


    x = 0
    y = 0

    screen.fill((0,0,0))
    if notPaused: 
        for row in a:
            for element in row:
                s = 0
                checkxmin = x-1
                checkymin = y-1
                checkxmax = x+1
                checkymax = y+1
                if checkxmin == -1: checkxmin = game_size - 1
                if checkxmax == game_size: checkxmax = 0
                if checkymin == -1: checkymin = game_size - 1
                if checkymax == game_size: checkymax = 0
                if a[checkxmin][checkymin]: s += 1
                if a[checkxmin][checkymax]: s += 1
                if a[checkxmax][checkymin]: s += 1
                if a[checkxmax][checkymax]: s += 1
                if a[x][checkymin]: s += 1
                if a[x][checkymax]: s += 1
                if a[checkxmin][y]: s += 1
                if a[checkxmax][y]: s += 1    
                if s == 1: pygame.draw.rect(screen,(25,25,25),(x*k_size+1,y*k_size+1,k_size-2,k_size-2)) 
                if s == 2: pygame.draw.rect(screen,(40,40,40),(x*k_size+1,y*k_size+1,k_size-2,k_size-2))
                if s >= 3: pygame.draw.rect(screen,(55,55,55),(x*k_size+1,y*k_size+1,k_size-2,k_size-2))  
                if a[x][y] and (s == 2 or s == 3): new_a[x][y] = True
                elif not a[x][y] and s == 3: new_a[x][y] = True
                else: new_a[x][y] = False 
                y += 1 
            x += 1
            y = 0
        x = 0  
        for row in a:
            for element in row:
                a[x][y] = new_a[x][y]
                new_a[x][y] = False
                y += 1 
            x += 1
            y = 0
        x = 0   
        time.sleep(0.05)

    for row in a:
        for element in row:
            if element: 
                pygame.draw.rect(screen,(255,255,255),(x*k_size+1,y*k_size+1,k_size-2,k_size-2))
            y += 1 
        x += 1
        y = 0
    x = 0    

    pygame.display.flip()
