# zip but not crypto

**Условие задания:**

> Мой друг думает что передавать информацию в зашифрованном архиве безопасно, благо у меня есть исходник его файла. Попробуй переубедить его.
>
> P.S. [7-zip](https://www.7-zip.org/) самый лучший архиватор (без шуток).

**Решение:**

Само название таска намекает нам что при шифровании использовался ZipCrypto, открыв архив через 7-zip можно в этом убедиться.

`Метод ZipCrypto Deflate`

Ищем в гугле, буквально вторая ссылка [bkcrack](https://github.com/kimci86/bkcrack). Устанавливаем как сказано в [readme.md](https://github.com/kimci86/bkcrack#install) и идём ломать.

Для восстановления ключа нам нужен открытый текст и зашифрованный, `game_of_life.py` есть и в архиве и в открытом виде.

```bash
$ ./bkcrack -C enc.zip -c game_of_life.py -p game_of_life.py
bkcrack 1.5.0 - 2023-10-04
Data error: ciphertext is smaller than plaintext.
```

Возвращается ошибка, попробуем сжать открытый текст с помощью [7-zip](https://www.7-zip.org/) (P.S. стандартное сжатие Windows вернёт ту же ошибку).

```bash
$ ./bkcrack -C enc.zip -c game_of_life.py -P game_of_life.zip -p game_of_life.py
bkcrack 1.5.0 - 2023-XX-XX
[XX:XX:XX] Z reduction using 738 bytes of known plaintext
100.0 % (738 / 738)
[XX:XX:XX] Attack on 12314 Z values at index 6
Keys: e1ceaa26 417beb7d b7d446b9
11.3 % (1392 / 12314)
[XX:XX:XX] Keys
e1ceaa26 417beb7d b7d446b9
```
В результате атаки получаем ключи `e1ceaa26 417beb7d b7d446b9` , которые будем использовать в дальнейшем.

Можно попробовать восстановить пароль. Однако времени на это не хватит.

```bash
$ ./bkcrack -C enc.zip -k e1ceaa26 417beb7d b7d446b9 -r 20 ?p
bkcrack 1.5.0 - 2023-XX-XX
[XX:XX:XX] Recovering password
length 0-6...
length 7...
length 8...
length 9...
length 10...
length 11...
length 12...
7.5 % (676 / 9025)
```

P.S. пароль - `Sup3r_H4rd_P455w0rd`

Поэтому просто запишем новый архив с другим паролем.

```bash
$ ./bkcrack -C enc.zip -k e1ceaa26 417beb7d b7d446b9 -U solved.zip password
bkcrack 1.5.0 - 2023-XX-XX
[XX:XX:XX] Writing unlocked archive solved.zip with password "password"
100.0 % (2 / 2)
Wrote unlocked archive.
```

Открываем архив с новым паролем.

Читаем флаг - `UralCTF{z1pcr4pt0?_m1st3r_KP4_s3nd5_h15_r3g4rd5}`.
